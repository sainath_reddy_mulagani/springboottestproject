package DevOps2.Mytolldemo2;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class Mytolldemo2Controller {

    @RequestMapping("/demo")
    public String helloworld() {
        return "This is hello world demo page";
    }
}
