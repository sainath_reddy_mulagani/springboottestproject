package DevOps2.Mytolldemo2;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Mytolldemo2Application {

	public static void main(String[] args) {
		SpringApplication.run(Mytolldemo2Application.class, args);
	}

}
